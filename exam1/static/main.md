# Examen de Bob

kjlsd -- oidjf

## Instructions

- durée: 2h
- pour rendre votre travail:
    - créez un dossier ``nom_prenom``
    - mettez les fichiers à rendre dans le dossier
    - ne mettez pas de fichiers superflus
    - archivez votre dossier au format ``tar.gz``
    - envoyez votre archive en utilisant le formulaire d'envoi, en bas de cette page

## Exercice 1

```haskell
add42 x = x + 42
```

## Exercice 2

![](bob.png)

## Exercice 3

Qu'en pensez-vous ?

![](greg.mp4)

