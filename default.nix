#{ pkgs ? import <nixpkgs> {} }:
{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/20.03.tar.gz") {} }:
let drv = pkgs.haskellPackages.callCabal2nix "autoexam" ./. {};
in if pkgs.lib.inNixShell then drv.env else drv

