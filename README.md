# autoexam

a web server that:

    - reads a Markdown file `static/main.md`
    - renders this file to HTML
    - adds a form for uploading files

![](demo.mp4)

