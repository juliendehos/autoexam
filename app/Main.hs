{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class (liftIO)
import Data.Maybe (fromMaybe)
import Data.Time (getCurrentTime, formatTime, defaultTimeLocale)
import Data.Time.LocalTime (getCurrentTimeZone, utcToLocalTime)
import Lucid
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import Network.Wai.Parse (fileContent, fileName)
import System.Directory (createDirectoryIfMissing, doesPathExist)
import System.Environment (lookupEnv)
import System.FilePath ((</>))
import System.Exit (exitFailure)
import Web.Scotty (files, get, html, middleware, post, scotty)


import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Char8 as BS
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import qualified Text.MMark as MM
import qualified Text.MMark.Extension as MM
import qualified Text.Megaparsec as M
import qualified Text.URI as URI

import Control.Lens
import Text.URI.Lens


paramStaticDir, paramUploadsDir, paramMainMd :: String
paramStaticDir = "static"
paramUploadsDir = "uploads"
paramMainMd = "main.md"

paramFavicon, paramCss :: T.Text
paramFavicon = "favicon.ico"
paramCss = "styles.css"


videoHtml :: T.Text -> Html ()
videoHtml uri =
    video_ [preload_ "metadata", controls_ ""]
        $ source_ [src_ uri , type_ "video/mp4"] 

videoExtension :: MM.Extension
videoExtension = MM.inlineRender $ \old inline ->
    case inline of
        (MM.Image _ uri _) ->
            case uri ^. uriPath of
                [] -> error "empty image URI"
                xs -> if ".mp4" `T.isSuffixOf` URI.unRText (last xs)
                      then videoHtml (T.intercalate "/" $ map URI.unRText xs)
                      else old inline
        other -> old other


readRenderMd :: FilePath -> IO (Html ())
readRenderMd pathname = do
    f <- TIO.readFile pathname
    case MM.parse pathname f of
        Left bundle -> do
            putStrLn $ M.errorBundlePretty bundle
            exitFailure
        Right r -> return $ MM.render $ MM.useExtension videoExtension r


uploadHtml :: Maybe T.Text -> Html ()
uploadHtml Nothing =
    p_ [style_ "color: darkgreen"] "ok : file sent successfully"
uploadHtml (Just err) = do
    p_ [style_ "color: red"] $ toHtml ("error : " <> err)
    p_ $ a_ [href_ "/"] "retry"


uploadAndHtml :: String -> BL.ByteString -> IO (Maybe T.Text)
uploadAndHtml filename filecontent = do
    let fullname = "uploads" </> filename
    exists <- doesPathExist fullname
    if exists
    then return $ Just "filename already exists"
    else BL.writeFile fullname filecontent >> return Nothing


myGetTimePrefix :: IO String
myGetTimePrefix = do
    tz <- getCurrentTimeZone
    tu <- getCurrentTime
    let tl = utcToLocalTime tz tu
    return $ formatTime defaultTimeLocale "%F_%H-%M-%S_" tl


renderPage :: Html () -> L.Text
renderPage contents = renderText $ do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [name_ "viewport",
                content_ "width=device-width, initial-scale=1, shrink-to-fit=no"]
            title_ "autoexam"
            link_ [rel_ "stylesheet", href_ paramCss]
            link_ [rel_ "shortcut icon", href_ paramFavicon]
        body_ contents


main :: IO ()
main = do
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    mainHtml <- readRenderMd $ paramStaticDir </> paramMainMd
    
    createDirectoryIfMissing False paramUploadsDir

    scotty port $ do
        middleware logStdoutDev
        middleware $ staticPolicy $ addBase paramStaticDir
        middleware $ gzip $ def { gzipFiles = GzipCompress }

        get "/" $ html $ renderPage $ do
            mainHtml
            hr_ []
            h1_ "Formulaire d'envoi"
            p_ "Vérifiez les instructions données en haut de la page !"
            form_ [method_ "post", enctype_ "multipart/form-data", action_ "/upload"] $ do
                input_ [type_ "file", name_ "myfile"]
                input_ [type_ "submit", value_ "send file"]

        post "/upload" $ do
            fs <- files
            err <- case fs of
                [(_, inp)] -> liftIO $ do
                    prefix <- myGetTimePrefix
                    let inpName = fileName inp
                        inpContent = fileContent inp
                        filename = prefix ++ BS.unpack inpName
                    if BS.empty == inpName || BL.empty == inpContent
                    then return $ Just "empty file/filename"
                    else uploadAndHtml filename inpContent
                _ -> return $ Just "send one file, please"
            html $ renderPage $ uploadHtml err

